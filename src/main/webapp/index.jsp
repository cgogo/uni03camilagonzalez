<%-- 
    Document   : index
    Created on : 04-05-2021, 1:55:18
    Author     : cGoGo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>UNI03 Camila Gonzalez</title>
    </head>
    <body>
        <h1>UNI03 Camila Gonzalez</h1>
        <ul>
            <li>
                <b>GET:</b>
                https://uni03camilagonzalez.herokuapp.com/api/alumno
                <br>
            </li>
            <li>
                <b>POST:</b>
                https://uni03camilagonzalez.herokuapp.com/api/alumno
                <p>Usar el formato</p>
                <pre>{"apellido":"","direccion":"","nombre":"","rut":"","telefono":""}</pre>
                <br>
            </li>
            <li>
                <b>PUT:</b>
                https://uni03camilagonzalez.herokuapp.com/api/alumno
                <p>Usar el formato</p>
                <pre>{"apellido":"","direccion":"","nombre":"","rut":"","telefono":""}</pre>
                <br>
            </li>
            <li>
                <b>DELETE:</b>
                https://uni03camilagonzalez.herokuapp.com/api/alumno/{iddelete}
                <p>Donde <b>{iddelete}</b> corresponde al rut</p>
            </li>
        </ul>
    </body>
</html>
