/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.uni03camilagonzalez;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.uni03camilagonzalez.dao.AlumnosJpaController;
import root.uni03camilagonzalez.dao.exceptions.NonexistentEntityException;
import root.uni03camilagonzalez.entity.Alumnos;

/**
 *
 * @author cGoGo
 */

@Path("alumno")
public class AlumnosUni {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarAlumnos() {
        
        AlumnosJpaController dao = new AlumnosJpaController();
        List<Alumnos> lista = dao.findAlumnosEntities();
        return Response.ok(200).entity(lista).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Alumnos alumno) {
        
        try {
            AlumnosJpaController dao = new AlumnosJpaController();
            dao.create(alumno);
        } catch (Exception ex) {
            Logger.getLogger(AlumnosUni.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(alumno).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete) {

        try {
            AlumnosJpaController dao = new AlumnosJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(AlumnosUni.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("Alumno eliminado").build();
    }
    
    @PUT
    public Response update(Alumnos alumno) {

        try {
            AlumnosJpaController dao = new AlumnosJpaController();
            dao.edit(alumno);
        } catch (Exception ex) {
            Logger.getLogger(AlumnosUni.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(alumno).build();
    }
}
